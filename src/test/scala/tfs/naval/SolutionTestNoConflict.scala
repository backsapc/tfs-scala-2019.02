package tfs.naval

import org.scalatest.{FlatSpec, Matchers}

import scala.util.Random

class SolutionTestNoConflict extends FlatSpec with Matchers {

  def generateShipsHorizontal(length: Int): List[List[(Int, Int)]] = {
    0.until(10).flatMap(i => 0.to(10 - length).map(s => s.until(length + s).map(j => (i, j)).toList)).toList
  }

  def generateShipsVertical(length: Int): List[List[(Int, Int)]] = {
    0.until(10).flatMap(i => 0.to(10 - length).map(s => s.until(length + s).map(j => (j, i)).toList)).toList
  }

  def actualGenerateShips(length: Int): List[List[(Int, Int)]] = generateShipsHorizontal(length) ++ generateShipsVertical(length)


  "ships" should "be linear" in {

    val ship = Lesson.input(0)._2
    Solution.validateShip(ship) shouldBe true

    val ship2 = List((1, 1), (1, 2), (2, 2))
    Solution.validateShip(ship2) shouldBe false
  }

  "Ship with negative position" should "be invalid" in {
    val ship = List((-1, 1))
    Solution.validateShip(ship) shouldBe false
  }

  "ships with length 1" should "be valid" in {
    val shipsToValidate = actualGenerateShips(1)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => t) shouldBe true
  }

  "ships with length 2" should "be valid" in {
    val shipsToValidate = actualGenerateShips(2)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => t) shouldBe true
  }

  "ships with length 3" should "be valid" in {
    val shipsToValidate = actualGenerateShips(3)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => t) shouldBe true
  }

  "ships with length 4" should "be valid" in {
    val shipsToValidate = actualGenerateShips(4)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => t) shouldBe true
  }

  "ships with length 5" should "be valid" in {
    val shipsToValidate = actualGenerateShips(5)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => !t) shouldBe true
  }

  "ships with length 6" should "be valid" in {
    val shipsToValidate = actualGenerateShips(6)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => !t) shouldBe true
  }

  "ships with length 7" should "be valid" in {
    val shipsToValidate = actualGenerateShips(7)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => !t) shouldBe true
  }

  "ships with length 8" should "be valid" in {
    val shipsToValidate = actualGenerateShips(8)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => !t) shouldBe true
  }

  "ships with length 9" should "be valid" in {
    val shipsToValidate = actualGenerateShips(9)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => !t) shouldBe true
  }

  "ships with length 10" should "be valid" in {
    val shipsToValidate = actualGenerateShips(10)
    shipsToValidate.map(s => Solution.validateShip(s)).forall(t => !t) shouldBe true
  }

  val emptyField: Vector[Vector[Boolean]] = Vector(
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
  )

  val nonEmptyField: Vector[Vector[Boolean]] = Vector(
    Vector(false, true, true, true, true, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
    Vector(false, false, false, false, false, false, false, false, false, false),
  )

  "ship's position" should "be valid on empty field" in {

    val ship = List((0, 1), (0, 2), (0, 3), (0, 4))

    Solution.validatePosition(ship, emptyField) shouldBe true
  }

  "ship's position" should "not be valid on non empty field" in {

    val ship = List((0, 1), (0, 2), (0, 3), (0, 4))

    Solution.validatePosition(ship, nonEmptyField) shouldBe false
  }

}

